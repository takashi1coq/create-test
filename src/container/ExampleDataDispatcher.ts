import { Dispatch } from 'redux'
import { ExampleDataActions, getExampleData } from 'src/module/exampleData/action'

export default class ExampleDataDispatcher {
  constructor(private dispatch: Dispatch<ExampleDataActions>) {
    this.dispatch = dispatch
  }

  public fetchExampleData(request: {}): void {
    this.dispatch(getExampleData(request))
  }
}
