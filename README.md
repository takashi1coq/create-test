create-test
==========

#### main package

- react
- react-dom
- react-redux

```
"react": "^16.12.0",
"react-dom": "^16.12.0",
```

##### types
```
"typescript": "^3.7.2",
"@types/react": "^16.9.13",
"@types/react-dom": "^16.9.4",
```

##### command
```
$ npm i react react-dom
$ npm i -D typescript @types/react @types/react-dom
```

#### webpack

- ts-loader
- webpack
- webpack-cli
- webpack-dev-server
- webpack-merge
- clean-webpack-plugin
- html-webpack-plugin

```
"ts-loader": "^6.2.1",
"webpack": "^4.41.2",
"webpack-cli": "^3.3.10",
"webpack-dev-server": "^3.9.0",
"webpack-merge": "^4.2.2"
"clean-webpack-plugin": "^3.0.0",
"html-webpack-plugin": "^3.2.0",
"file-loader": "^5.0.2",
```

##### command
```
$ npm i -D ts-loader webpack webpack-cli webpack-dev-server webpack-merge clean-webpack-plugin html-webpack-plugin file-loader
```

#### eslint + prettier

- prettier
  - 本体
- pretty-quick
  - 変更されたファイルだけ実行してquick
- eslint-config-prettier
  - prettierと競合するeslintのルールを無視する
- eslint-plugin-prettier
  - eslintのルールでprettierを実行する → ルールを無視してルールで実行する??

```
"prettier": "^1.19.1",
"pretty-quick": "^2.0.1",
"eslint-config-prettier": "^6.7.0",
"eslint-plugin-prettier": "^3.1.1",
```

##### command
```
$ npm i -D prettier pretty-quick eslint-config-prettier eslint-plugin-prettier
```

#### eslint + webpack

- eslint-import-resolver-webpack
  - webpackにてimportPathをいい感じにしている場合必要

```
"eslint-import-resolver-webpack": "^0.11.1",
```

#### command
```
$ npm i -D eslint-import-resolver-webpack
```

#### eslint-airbnb

- eslint-config-airbnb + (dependencies)
  - eslintのルールの拡張版
  - 依存関係に注意しながらインストールする必要がある
  - ついでにeslintもインストールする

```
"eslint-config-airbnb": "^18.0.1",
"eslint": "^6.7.1",
"eslint-plugin-import": "^2.18.2",
"eslint-plugin-jsx-a11y": "^6.2.3",
"eslint-plugin-react": "^7.16.0",
"eslint-plugin-react-hooks": "^2.3.0", ←　アウト
```

##### command

- 依存関係を解決しながらインストール
```
$ npx install-peerdeps --dev eslint-config-airbnb

*2019-12-18
+ eslint-plugin-jsx-a11y@6.2.3
+ eslint-plugin-import@2.19.1
+ eslint-plugin-react-hooks@1.7.0
+ eslint-config-airbnb@18.0.1
+ eslint-plugin-react@7.17.0
+ eslint@6.1.0
```

- 依存関係だけを確認
```
$ npm info "eslint-config-airbnb@latest" peerDependencies
```

#### typescript + eslint

- @typescript-eslint/eslint-plugin
- @typescript-eslint/parser

```
"@typescript-eslint/eslint-plugin": "^2.8.0",
"@typescript-eslint/parser": "^2.8.0",
```

##### command
```
$ npm i -D @typescript-eslint/eslint-plugin @typescript-eslint/parser
```

#### material-ui

- @material-ui/core
- @material-ui/icons

```
"@material-ui/core": "^4.7.0",
"@material-ui/icons": "^4.5.1",
```

##### command
```
$ npm i @material-ui/core @material-ui/icons
```

#### react-redux

- react-redux

```
"redux": "^4.0.4",
"react-redux": "^7.1.3",
```

##### types
```
"@types/react-redux": "^7.1.5",
```

##### command
```
$ npm i redux react-redux
$ npm i -D @types/react-redux
```

#### react-router-dom

- react-router-dom

```
"react-router-dom": "^5.1.2",
```

##### typs
```
"@types/react-router-dom": "^5.1.2",
```


##### command
```
$ npm i react-router-dom
$ npm i -D @types/react-router-dom
```

#### redux-persist

- redux-persist

```
"redux-persist": "^6.0.0",
```

##### command
```
$ npm i redux-persist
```

#### saga and axios

- redux-saga
- @redux-saga/core
- axios

```
    "redux-saga": "^1.1.3"
    "@redux-saga/core": "^1.1.3",
    "axios": "^0.19.0",
```

##### command
```
$ npm i redux-saga @redux-saga/core axios
```

#### other

```
"moment": "^2.24.0",
"recharts": "^1.8.5",
"redux-logger": "^3.0.6",
"@types/recharts": "^1.8.3",
"@types/redux-logger": "^3.0.7",
```

##### command

```
$ npm i moment recharts redux-logger
$ npm i -D @types/recharts @types/redux-logger
```
