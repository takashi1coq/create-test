import axios from 'axios'
import { put, call, takeEvery } from 'redux-saga/effects'
import { IGetExampleData, ActionName } from 'src/module/exampleData/action'

const GET_EXAMPLE_DATA_PATH = 'http://localhost:3000/test'

// post request
export const postApi = async (url: string, data: {}) => {
  return axios
    .post(url, data)
    .then(response => {
      const successResult = response.data
      return { successResult }
    })
    .catch(error => {
      return { error }
    })
}

export function* exampleDataGet(action: IGetExampleData) {
  const { successResult, error } = yield call(postApi, GET_EXAMPLE_DATA_PATH, action.requestData)
  if (successResult) {
    yield put({ type: ActionName.SUCCESS_GET_EXAMPLE_DATA, gotItem: successResult })
  } else {
    yield put({ type: ActionName.ERROR_GET_EXAMPLE_DATA, errorItem: error })
  }
}
export function* exampleDataGetSaga() {
  yield takeEvery(ActionName.GET_EXAMPLE_DATA, exampleDataGet)
}
