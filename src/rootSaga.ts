import { call, all } from 'redux-saga/effects'
import { exampleDataGetSaga } from 'src/module/exampleData/saga'

export default function* rootSaga() {
  yield all([call(exampleDataGetSaga)])
}
