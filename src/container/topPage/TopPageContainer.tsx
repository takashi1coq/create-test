import React, { ReactElement } from 'react'
import { IExampleState } from 'src/module/exampleData/reducer'
import ExampleDataDispatcher from 'src/container/ExampleDataDispatcher'
import { useSelector, useDispatch } from 'react-redux'
import { ReduxState } from 'src/store'
import DashBoard from 'src/view/topPage/DashBoard'

export interface ITopPageValue {
  dataValue: IExampleState
}
export interface ITopPageDispatcher {
  dataDispatcher: ExampleDataDispatcher
}

const TopPageContainer: React.FC = (): ReactElement => {
  const topPageValue: ITopPageValue = {
    dataValue: useSelector<ReduxState, IExampleState>(state => state.exampleData),
  }
  const topPageDispatcher: ITopPageDispatcher = {
    dataDispatcher: new ExampleDataDispatcher(useDispatch()),
  }
  return <DashBoard value={topPageValue} dispatcher={topPageDispatcher} />
}
export default TopPageContainer
