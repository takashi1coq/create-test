import { exampleDataReducer, IExampleState } from 'src/module/exampleData/reducer'
import { ExampleDataActions } from 'src/module/exampleData/action'
import { Action, createStore, combineReducers, applyMiddleware } from 'redux'
import createSagaMiddleware from '@redux-saga/core'
import logger from 'redux-logger'
import { persistStore, persistReducer } from 'redux-persist'
import storageSession from 'redux-persist/lib/storage/session'
import rootSaga from 'src/rootSaga'

const persistConfig = {
  key: 'root',
  storage: storageSession,
}

const configureStore = () => {
  const sagaMiddleware = createSagaMiddleware()
  const store = createStore(
    persistReducer(
      persistConfig,
      combineReducers({
        exampleDataReducer,
      }),
    ),
    applyMiddleware(logger, sagaMiddleware),
  )
  sagaMiddleware.run(rootSaga)
  const persistor = persistStore(store)
  return { store, persistor }
}

export default configureStore

export type ReduxState = {
  exampleData: IExampleState
}

export type ReduxAction = Action | ExampleDataActions
