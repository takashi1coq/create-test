import { ExampleDataActions, ActionName } from 'src/module/exampleData/action'

export interface IExampleState {
  name: string
  price: number
  achievement: number
}

export interface IErrorExampleState {
  message: string
  stack: string
}

export interface IExampleDataState {
  gotState: IExampleState[]
  errorState: IErrorExampleState
  didGet: boolean
}

export const initialState: IExampleDataState = {
  gotState: [],
  errorState: {
    message: '',
    stack: '',
  },
  didGet: false,
}

export const exampleDataReducer = (
  state: IExampleDataState = initialState,
  action: ExampleDataActions,
): IExampleDataState => {
  switch (action.type) {
    case ActionName.SUCCESS_GET_EXAMPLE_DATA: {
      return {
        ...state,
        gotState: action.gotItem,
      }
    }
    case ActionName.ERROR_GET_EXAMPLE_DATA: {
      return {
        ...state,
        errorState: action.errorInfo,
      }
    }
    default: {
      return state
    }
  }
}
