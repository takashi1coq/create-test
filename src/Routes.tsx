import React, { ReactElement } from 'react'
import { ThemeProvider, createMuiTheme } from '@material-ui/core/styles'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import TopPageContainer from 'src/container/topPage/TopPageContainer'

const mainTheme = createMuiTheme({
  palette: {
    primary: {
      main: '#d2691e',
    },
    secondary: {
      main: '#ffd700',
    },
  },
  typography: {
    fontSize: 13,
  },
})

const enum ROUTE_PATHS {
  home = '/',
}

const Routes: React.FC = (): ReactElement => {
  return (
    <ThemeProvider theme={mainTheme}>
      <BrowserRouter>
        <Switch>
          <Route path={ROUTE_PATHS.home} exact component={TopPageContainer} />
        </Switch>
      </BrowserRouter>
    </ThemeProvider>
  )
}
export default Routes
