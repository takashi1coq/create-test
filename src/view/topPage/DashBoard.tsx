import React, { ReactElement } from 'react'
import { ITopPageValue, ITopPageDispatcher } from 'src/container/topPage/TopPageContainer'
import { Button } from '@material-ui/core'

export interface ITopPageDashBoardProps {
  value: ITopPageValue
  dispatcher: ITopPageDispatcher
}

const DashBoard: React.FC<ITopPageDashBoardProps> = (props): ReactElement => {
  const {dispatcher} = props
  return (
    <>
      <Button onClick={() => dispatcher.dataDispatcher.fetchExampleData({name: 'aaa'})}>post</Button>
    </>
  )
}
export default DashBoard
