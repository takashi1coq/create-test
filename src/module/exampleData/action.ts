import { Action } from 'redux'
import { IExampleState, IErrorExampleState } from 'src/module/exampleData/reducer'

export const enum ActionName {
  GET_EXAMPLE_DATA = 'exampleData/get_example_data',
  SUCCESS_GET_EXAMPLE_DATA = 'exampleData/success_get_example_data',
  ERROR_GET_EXAMPLE_DATA = 'exampleData/error_get_example_data',
  INITIAL_EXAMPLE_STATE = 'exampleData/initial_example_state',
}

export interface IGetExampleData extends Action {
  type: ActionName.GET_EXAMPLE_DATA
  requestData: {}
}
export const getExampleData = (obj: {}): IGetExampleData => ({
  type: ActionName.GET_EXAMPLE_DATA,
  requestData: obj,
})

export interface ISuccessGetExampleData extends Action {
  type: ActionName.SUCCESS_GET_EXAMPLE_DATA
  gotItem: IExampleState[]
}
export const successGetExampleData = (item: IExampleState[]): ISuccessGetExampleData => ({
  type: ActionName.SUCCESS_GET_EXAMPLE_DATA,
  gotItem: item,
})

export interface IErrorGetExampleData extends Action {
  type: ActionName.ERROR_GET_EXAMPLE_DATA
  errorInfo: IErrorExampleState
}
export const errorGetExampleData = (obj: IErrorExampleState): IErrorGetExampleData => ({
  type: ActionName.ERROR_GET_EXAMPLE_DATA,
  errorInfo: obj,
})

export interface IInitialExampleDataState extends Action {
  type: ActionName.INITIAL_EXAMPLE_STATE
}
export const initialExampleDataState = (): IInitialExampleDataState => ({
  type: ActionName.INITIAL_EXAMPLE_STATE,
})

export type ExampleDataActions =
  | IGetExampleData
  | ISuccessGetExampleData
  | IErrorGetExampleData
  | IInitialExampleDataState
